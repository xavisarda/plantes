
CREATE TABLE plant (
  plid int(11) NOT NULL AUTO_INCREMENT,
  nom varchar(255) NOT NULL,
  nom_c varchar(255) NOT NULL,
  tipus int(11) NOT NULL,
  perenne tinyint(1) DEFAULT NULL,
  PRIMARY KEY (plid));

CREATE TABLE `plant_type` (
  `plt_id` int(11) NOT NULL AUTO_INCREMENT,
  `t_name` varchar(255) NOT NULL,
  PRIMARY KEY (`plt_id`)); 

alter table plant add foreign key (tipus) references plant_type(plt_id);

CREATE VIEW plant_complete 
  AS select plid, nom, nom_c, tipus, t_name, perenne from plant, plant_type where tipus = plt_id;

INSERT INTO `plant_type` VALUES (1,'Aromatica'),(2,'Cactus');

INSERT INTO `plant` VALUES (1,'Romani','Salvia rosmarinus',1,1),(2,'Aloe','Aloe Chinensis',2,1);
