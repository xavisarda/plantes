<?php

require_once(__DIR__.'/../../app/inc/constants.php');
require_once(__DIR__.'/../../app/controller/PlantController.php');

//RECUPEREM DADES
$plname = $_POST['pln'];
$plsciname = $_POST['plnc'];
$pltype = $_POST['plt'];
$pper = isset($_POST['plp']) ? $_POST['plp'] : NULL;

$cnt = new PlantController();
$plant = $cnt->addPlant($plname, $plsciname, $pltype, $pper);

?><html>
  <head>
    <title>Added Plant</title>
  </head>
  <body>
    <h1>New Plant: <?=$plant->getName()?> (<?=$plant->getSciName()?>)</h1>
    <ul>
      <li><?=$plant->getType()?>
    </ul>
    <a href="/">Back to home</a>
  </body>
</html>