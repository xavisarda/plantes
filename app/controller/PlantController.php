<?php

require_once(__DIR__.'/../model/db/PlantDb.php');

class PlantController{

  public function listPlants(){
      $db = new PlantDb();
      return $db->listPlants();
  }

  public function getPlant($plant_id){
      $db = new PlantDb();
      return $db->getPlant($plant_id);
  }

  public function addPlant($n, $nc, $t, $p){
    $db = new PlantDb();
    if($p == 'true' || $p == 'TRUE'){
      $p = 1;
    }else{
      $p = 0;
    }
    return $db->addPlant($n, $nc, $t, $p);
  }
  public function updatePlant($n, $nc, $t, $p, $id){
    $db = new PlantDb();
    if($p == 'true' || $p == 'TRUE'){
      $p = 1;
    }else{
      $p = 0;
    }
    return $db->updatePlant($n, $nc, $t, $p, $id);
  }

  public function deletePlant($id){
    $db = new PlantDb();
    return $db->removePlant($id);
  }

  public function getPlantTypes(){
    $db = new PlantDb();
    return $db->listPlantTypes();
  }

}