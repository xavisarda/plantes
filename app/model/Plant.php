<?php 

class Plant{

  private $_pid;

  private $_name;
  private $_sciname;
  private $_type;
  private $_typeid;
  private $_cad;

  public function __construct($n, $sc, $t, $ti, $c, $i = null){
    $this->setName($n);
    $this->setSciName($sc);
    $this->setType($t);
    $this->setTypeId($ti);
    $this->setCad($c);
    if($i != null){
      $this->setId($i);
    }
  }

  public function getId(){
    return $this->_pid;
  }

  public function getName(){
    return $this->_name;
  }

  public function getSciName(){
    return $this->_sciname;
  }

  public function getType(){
    return $this->_type;
  }

  public function getTypeId(){
    return $this->_typeid;
  }

  public function getCad(){
    return $this->_cad;
  }

  public function setId($v){
    $this->_pid = $v;
  }

  public function setName($v){
    $this->_name = $v;
  }

  public function setSciName($v){
    $this->_sciname = $v;
  }

  public function setType($v){
    $this->_type = $v;
  }

  public function setTypeId($v){
    $this->_typeid = $v;
  }

  public function setCad($v){
    $this->_cad = $v;
  }

}
