<?php 

require_once(__DIR__.'/../../inc/constants.php');
require_once(__DIR__.'/../Plant.php');

class PlantDb{

    private $_conn;

    public function listPlants(){
      $this->openConnection();
         
      $query = "SELECT * FROM plant_complete";
      $stmt = $this->_conn->prepare($query);
        
      $stmt->execute();
      $res = $stmt->get_result();
        
      $plants = array();
      while ($plant = $res->fetch_assoc() ) {
        array_push($plants, new Plant($plant['nom'], $plant['nom_c'],
            $plant['t_name'], $plant['tipus'], $plant['perenne'] == 1 ? true : false, $plant['plid']));
      }
      return $plants;
    }

    public function listPlantTypes(){
      $this->openConnection();
         
      $query = "SELECT * FROM plant_type";
      $stmt = $this->_conn->prepare($query);
        
      $stmt->execute();
      $res = $stmt->get_result();
        
      $plants = array();
      while ($plant = $res->fetch_assoc() ) {
        $pl = [ "idtype" => $plant['plt_id'], "nametype" => $plant['t_name']];
        array_push($plants, $pl);
      }
      return $plants;
    }

    public function getPlant($id){
      $this->openConnection();
         
      $query = "SELECT * FROM plant_complete WHERE plid = ?";
      $stmt = $this->_conn->prepare($query);

      $stmt->bind_param("i", $i);
      $i = $id;
        
      $stmt->execute();
      $res = $stmt->get_result();
        
      $plant = $res->fetch_assoc();
      return new Plant($plant['nom'], $plant['nom_c'],
            $plant['t_name'], $plant['tipus'], $plant['perenne'] == 1 ? true : false, $plant['plid']);
    }

    public function addPlant($n, $nc, $t, $p){
      $this->openConnection();
      $query = "INSERT INTO plant (nom, nom_c, tipus, perenne) VALUES (?, ?, ?, ?)";
      $stmt = $this->_conn->prepare($query);

      $stmt->bind_param("ssis", $nom, $nomc, $type, $per);
      $nom = $n;
      $nomc = $nc;
      $type = $t;
      $per = $p;
        
      $stmt->execute();
      //echo($stmt->insert_id);
      return $this->getPlant($stmt->insert_id);
    }

    public function updatePlant($n, $nc, $t, $p, $id){
      $this->openConnection();
      $query = "UPDATE plant SET nom = ? , nom_c = ?, tipus = ?, perenne = ? WHERE plid = ?";
      $stmt = $this->_conn->prepare($query);

      $stmt->bind_param("ssiii", $nom, $nomc, $type, $per, $i);
      $nom = $n;
      $nomc = $nc;
      $type = $t;
      $per = $p;
      $i = $id;
        
      $stmt->execute();
      //echo($stmt->insert_id);
      return $this->getPlant($id);
    }

    public function removePlant($id){
      $plant = $this->getPlant($id);

      $this->openConnection();
         
      $query = "DELETE FROM plant WHERE plid = ?";
      $stmt = $this->_conn->prepare($query);

      $stmt->bind_param("i", $i);
      $i = $id;
        
      $stmt->execute();
      return $plant;
    }

    private function openConnection(){
      if($this->_conn == NULL){
        $this->_conn = mysqli_connect(DB_HOST, DB_USER, DB_PWD, DB_DB);    
      }
    }
    
}
